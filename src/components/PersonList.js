import axios from 'axios'
import React from 'react'

class PersonList extends React.Component {
    state = {
        persons: [],
    };

    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/users').then(res => {
              console.log(res)
             this.setState({ persons: res.data});
        });
    }


    render() {
        return(
            <div>
               <ul>
                   {this.state.persons.map(personal => <li key={personal.id}>{personal.name}{personal.username}</li>)}
               </ul>
            </div>
        )
    }
}

export default PersonList;

