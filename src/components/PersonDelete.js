import axios from 'axios';
import React from 'react';
import './style.css'

class PersonDelete extends React.Component {

    state = {
        id: 0,
    };
 
    handleChange = event => {
     this.setState({ id: event.target.value})
    };
 
 
    handleSubmit = event => {
        event.preventDefault();
 

 
        axios.delete(`https://jsonplaceholder.typicode.com/users/${this.state.id}`)
        .then(res => {
            console.log(res);
            console.log (res.data);
        });
    };

  render() {
    return (
      <div> 
        <form onSubmit={this.handleSubmit}>
                <label>
                    Person ID:
                    <input type="number" name="id" onChange={this.handleChange} /> 
                </label>
                <button className="btn__clickS">Delete</button>
        </form>
         
      </div>
    );
  }
}

export default  PersonDelete;
