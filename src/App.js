import logo from './logo.svg';
import './App.css';
import PersonList from './components/PersonList';
import PersonInput from './components/PersonInput';
import PersonDelete from './components/PersonDelete';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
           Consuming API with AXIOS
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
        </a>
      </header>
      <PersonList />
      <PersonInput />
      <PersonDelete />
    </div>
  );
}

export default App;
